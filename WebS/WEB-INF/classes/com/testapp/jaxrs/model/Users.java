package com.testapp.jaxrs.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="person")
public class Users {
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private String birthday;
	private int id;

	public String getfirstName() {
		return firstname;
	}
	
	public void setfirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getlastname() {
		return lastname;
	}
	
	public void setlastname(String lastname) {
		this.lastname = lastname;
	}

	public String getemail() {
		return email;
	}

	public void setemail(String email) {
		this.email = email;
	}
	
	public String getpassword() {
		return password;
	}

	public void setpassword(String password) {
		this.password= password;
	}
	public String gebirthday() {
		return birthday;
	}

	public void setbirthday(String birthday) {
		this.birthday= birthday;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString(){
		return id+"::"+firstname+"::"+lastname+"::"+email+"::"+birthday;
	}
}
