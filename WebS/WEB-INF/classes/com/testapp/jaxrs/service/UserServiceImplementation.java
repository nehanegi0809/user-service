package com.testapp.jaxrs.service;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.testapp.jaxrs.model.Users;
import com.testapp.jaxrs.model.Response;

@Path("/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)

public class UserServiceImplementation implements UserService {

	private static Map<Integer,Users> users = new HashMap<Integer,Users>();
	
	@Override
	@POST
    @Path("/add")
	public Response addUser(Users p)
	{
		Response response = new Response();
		if(users.get(p.getId()) != null){
			response.setStatus(false);
			response.setMessage("User Already Exists");
			return response;
		}
		users.put(p.getId(), p);
		response.setStatus(true);
		response.setMessage("User created successfully");
		return response;
	}

	@Override
	@GET
    @Path("/{id}/modify")
	public Response modifyUser(@PathParam("id") int id) {
		Response response = new Response();
		if(users.get(id) == null){
			response.setStatus(false);
			response.setMessage("User Doesn't Exists");
			return response;
		}
		users.remove(id);
		response.setStatus(true);
		response.setMessage("User deleted sucessfully");
		return response;
	}

	@Override
	@GET
	@Path("/{id}/get")
	public Users getUser(@PathParam("id") int id) {
		return users.get(id);
	}
	
	@GET
	@Path("/{id}/getDummy")
	public Users getDummyUser(@PathParam("id") int id) {
		Users p = new Users();
		p.setfirstname("Dummy");
		p.setlastname("Dummy");
		p.setemail("Dummy");
		p.setpassword("*********");
		p.setbirthday("Dummy");
		p.setId(id);
		return p;
	}

	@Override
	@GET
	@Path("/getAll")
	public Users[] getAllUsers() {
		Set<Integer> ids = users.keySet();
		Users[] p = new Users[ids.size()];
		int i=0;
		for(Integer id : ids){
			p[i] = users.get(id);
			i++;
		}
		return p;
	}

}