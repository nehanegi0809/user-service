package com.testapp.jaxrs.service;

import com.testapp.jaxrs.model.Users;
import com.testapp.jaxrs.model.Response;

public interface UserService {

	public Response addUser(Users p);
	
	public Response modifyUser(int id);
	
	
	public Users getUser(int id);
	
	public Users[] getAllUsers();

}